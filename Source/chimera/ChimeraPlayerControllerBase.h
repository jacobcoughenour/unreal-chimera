﻿#pragma once

#include "chimera.h"
#include "GameFramework/PlayerController.h"
#include "ChimeraPlayerControllerBase.generated.h"

UCLASS()
class CHIMERA_API AChimeraPlayerControllerBase : public APlayerController {

	GENERATED_BODY()

public:

	AChimeraPlayerControllerBase() {}

};
