// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/SkyAtmosphereComponent.h"
#include "Components/SkyLightComponent.h"
#include "Components/DirectionalLightComponent.h"

#include "CoreMinimal.h"
#include "PlanetGenerator.h"
#include "GameFramework/Actor.h"
#include "VoxelSpawners/VoxelSpawnerConfig.h"
#include "VoxelWorldGenerators/VoxelWorldGeneratorPicker.h"
#include "VoxelData/VoxelData.h"

#include "Planet.generated.h"

USTRUCT(BlueprintType)
struct FVoxelHitResult {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FIntVector VoxelPosition = FIntVector(ForceInit);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float VoxelDensity = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector ImpactPoint = FVector(ForceInit);

};


UCLASS()
class CHIMERA_API APlanet : public AActor {
	GENERATED_BODY()


public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float VoxelSize = 64;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UVoxelMaterialCollectionBase* MaterialCollectionLiquid;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UVoxelMaterialCollectionBase* MaterialCollection;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, AdvancedDisplay)
	bool bDisableOnScreenMessages = false;

	// TODO make a custom picker for only picking planet generators
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Planet - Generator")
	FVoxelWorldGeneratorPicker PlanetGenerator;

	// TODO the seed and generator settings should be determined by a global seed

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Planet - Generator")
	int32 GeneratorNoiseSeed = 1337;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Planet - Generator")
	int32 GeneratorNoiseFrequency = 4;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Planet - Generator")
	int32 GeneratorNoiseExponent = 2;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Planet - Generator")
	int32 GeneratorSecondaryNoiseFrequency = 4;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Planet - Generator")
	int32 GeneratorNoiseOctaves = 4;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Planet - Generator")
	int32 GeneratorSecondaryNoiseOctaves = 4;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Planet - Generator")
	int32 GeneratorSeaLevel = 512;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Planet - Generator")
	int32 GeneratorTerrainHeight = 64;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Planet - Generator")
	int32 GeneratorSecondaryTerrainHeight = 32;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Planet - Gravity")
	float SeaLevelGravity = 980.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Planet - Spawners")
	UVoxelSpawnerConfig* SpawnerConfig;


	// Sets default values for this actor's properties
	APlanet();

	AVoxelWorld* PlanetLayerActors[PlanetLayerType::PLANET_LAYER_COUNT];

	FVoxelFastNoise FoliageNoise;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class UDirectionalLightComponent* DirectionalLightComponent;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class USkyAtmosphereComponent* SkyAtmosphereComponent;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class USkyLightComponent* SkyLightComponent;

	// called right before CreateWorld() is called for this layer
	UFUNCTION(BlueprintNativeEvent)
	void OnPlanetLayerSpawned(const PlanetLayerType& LayerType, AVoxelWorld* LayerWorld);
	virtual void OnPlanetLayerSpawned_Implementation(const PlanetLayerType& LayerType, AVoxelWorld* LayerWorld);

	// called right after CreateWorld() is called for this layer
	UFUNCTION(BlueprintNativeEvent)
	void OnPlanetLayerCreated(const PlanetLayerType& LayerType, AVoxelWorld* LayerWorld);
	virtual void OnPlanetLayerCreated_Implementation(const PlanetLayerType& LayerType, AVoxelWorld* LayerWorld);

	UFUNCTION(BlueprintNativeEvent)
	void OnVoxelChunkActivate(const PlanetLayerType& LayerType, AVoxelWorld* LayerWorld, const FVoxelIntBox& Bounds);
	virtual void OnVoxelChunkActivate_Implementation(const PlanetLayerType& LayerType, AVoxelWorld* LayerWorld,
													const FVoxelIntBox& Bounds);

	UFUNCTION(BlueprintNativeEvent)
	void OnVoxelChunkDeactivate(const PlanetLayerType& LayerType, AVoxelWorld* LayerWorld, const FVoxelIntBox& Bounds);
	virtual void OnVoxelChunkDeactivate_Implementation(const PlanetLayerType& LayerType, AVoxelWorld* LayerWorld,
														const FVoxelIntBox& Bounds);

	UFUNCTION(BlueprintNativeEvent)
	void OnVoxelChunkGenerate(const PlanetLayerType& LayerType, AVoxelWorld* LayerWorld, const FVoxelIntBox& Bounds);
	virtual void OnVoxelChunkGenerate_Implementation(const PlanetLayerType& LayerType, AVoxelWorld* LayerWorld,
													const FVoxelIntBox& Bounds);

	UFUNCTION(BlueprintNativeEvent)
	void SpawnTree(AVoxelWorld* LayerWorld, const FIntVector& VoxelPoint, const FVector& SurfaceNormal);
	virtual void SpawnTree_Implementation(AVoxelWorld* LayerWorld, const FIntVector& VoxelPoint,
										const FVector& SurfaceNormal);

	UFUNCTION(BlueprintCallable)
	bool LineTraceVoxels(struct FVoxelHitResult& OutHit, AVoxelWorld* LayerWorld, const FVector& Start,
						const FVector& End) const;

	template <typename UnaryPredicate>
	bool LineTraceVoxelsByPredicate(struct FVoxelHitResult& OutHit, AVoxelWorld* LayerWorld, const FVector& Start,
									const FVector& End, const UnaryPredicate& Pred) const;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;
	virtual void DestroyLayers();

	virtual void OnConstruction(const FTransform& Transform) override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/**
	* @brief Calculates the sum of all gravitational forces from all the planets acting on this point.
	* @param WorldContextObject World context object used for getting the planets list from the game instance.
	* @param Point Point in space to sample.
	* @return The sum of all gravitational forces from all the planets acting on this point.
	*/
	UFUNCTION(BlueprintPure)
	static FVector GetPlanetaryGravityAtPoint(const UObject* WorldContextObject, FVector Point);

};
