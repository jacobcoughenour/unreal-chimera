﻿#include "ChimeraEditorModule.h"

#include "AssetToolsModule.h"
#include "IAssetTools.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_GAME_MODULE(FChimeraEditorModule, ChimeraEditor);

#define LOCTEXT_NAMESPACE "FChimeraEditorModule"

void FChimeraEditorModule::StartupModule() {

	// IAssetTools& AssetTools = FModuleManager::LoadModuleChecked<FAssetToolsModule>("AssetTools").Get();

	// ChimeraAssetCategory = AssetTools.RegisterAdvancedAssetCategory(FName(TEXT("ChimeraCategory")),
	// 																LOCTEXT("ChimeraCategory", "Chimera"));
	//
	// {
	// 	TSharedRef<IAssetTypeActions> ACT_UItemAsset = MakeShareable(new FAssetTypeActions_ItemAsset);
	// 	AssetTools.RegisterAssetTypeActions(ACT_UItemAsset);
	//
	// 	TSharedRef<IAssetTypeActions> ACT_UItemDefinitionTable = MakeShareable(
	// 		new FAssetTypeActions_ItemDefinitionTable);
	// 	AssetTools.RegisterAssetTypeActions(ACT_UItemDefinitionTable);
	// }

}

#undef LOCTEXT_NAMESPACE
