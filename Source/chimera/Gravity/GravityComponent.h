// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GravityComponent.generated.h"


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class CHIMERA_API UGravityComponent : public UActorComponent {
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UGravityComponent();

	virtual void InitializeComponent() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
								FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintPure)
	FVector GetGravity() const {
		return Gravity;
	}

	UFUNCTION(BlueprintPure)
	FVector GetUpDirection() const {
		return Gravity.GetSafeNormal() * -1.0f;
	}

	UFUNCTION(BlueprintPure)
	bool IsEffectedByGravity() const {
		return !Gravity.IsNearlyZero();
	}

	UFUNCTION(BlueprintPure)
	UPrimitiveComponent* GetCollisionPrimitiveComponent() const {
		return CollisionPrimitiveComponent;
	}

protected:

	UPROPERTY()
	UPrimitiveComponent* CollisionPrimitiveComponent;

	UPROPERTY()
	FVector Gravity;

};
