﻿#include "ItemContainer.h"

int32 UItemContainer::GetItemCountInArray(TArray<UItemStack*> Items) {
	int32 Count = 0;

	for (auto ItemStack : Items) {
		if (ItemStack != nullptr)
			Count += ItemStack->GetAmount();
	}

	return Count;
}

void UItemContainer::SetPanelName(const FText NewPanelName) {
	PanelName = NewPanelName;

	// todo: emit some event here
}

bool UItemContainer::SetCapacity(int32 NewCapacity) {
	if (TotalItemAmount > NewCapacity)
		return false;

	Capacity = NewCapacity;
	return true;
}

int32 UItemContainer::ItemCount() const {
	return Contents.Num();
}

bool UItemContainer::IsFull() const {
	return TotalItemAmount == Capacity;
}

int32 UItemContainer::RemainingCapacity() const {
	return Capacity - TotalItemAmount;
}

bool UItemContainer::CanAcceptItem(const UItem* Item) const {

	// container is full
	if (IsFull())
		return false;

	// todo reject items that this container does not support eg: a fluid container should not accept solid items

	return true;
}

bool UItemContainer::IsValidIndex(const int32 SlotIndex) const {
	return Contents.IsValidIndex(SlotIndex);
}

UItemStack* UItemContainer::GetItemStackAtIndex(const int32 SlotIndex) const {
	return Contents[SlotIndex];
}

bool UItemContainer::TryGetItemStackAtIndex(const int32 SlotIndex, UItemStack*& OutItemStack) {
	if (!IsValidIndex(SlotIndex))
		return false;
	OutItemStack = Contents[SlotIndex];
	return OutItemStack != nullptr;
}

int32 UItemContainer::FindFirstStackableIndex(const UItem* Item) const {

	if (!Item->IsStackable)
		return INDEX_NONE;

	return Contents.IndexOfByPredicate([Item](const UItemStack* CurrentItem) {
		return Item == CurrentItem->GetItem();
	});
}

bool UItemContainer::AddItemStack(FItemContainerItemTransferResult& OutResult, UItemStack* ItemStack,
								const bool AllowPartial) {

	// invalid parameters
	if (ItemStack->GetAmount() < 1 || (!ItemStack->GetItem()->IsStackable && ItemStack->GetAmount() > 1))
		return false;

	// target container can not accept the item
	if (!CanAcceptItem(ItemStack->GetItem()))
		return false;

	auto AmountToTransfer = ItemStack->GetAmount();
	const auto Remaining = RemainingCapacity();

	// not enough remaining space for the amount
	if (AmountToTransfer > Remaining) {

		if (!AllowPartial)
			return false;

		// we need to split the stack
		OutResult.TransferredPartial = true;
		AmountToTransfer = Remaining;
	}

	if (!AddItemStack_Internal(OutResult, ItemStack, AmountToTransfer))
		return false;

	FItemContainerSlotChangeInfo ChangeInfo;

	ChangeInfo.Type = EItemContainerSlotChangeType::Item_Added;
	ChangeInfo.SourceContainer = nullptr;
	ChangeInfo.SourceSlotIndex = INDEX_NONE;
	ChangeInfo.DestinationContainer = this;
	ChangeInfo.DestinationSlotIndex = OutResult.SlotIndex;

	NotifyContainerSlotChanged(ChangeInfo);

	return true;

}


bool UItemContainer::AddItemStack_Internal(FItemContainerItemTransferResult& OutResult, UItemStack* ItemStack,
											const int Amount) {

	// search for a slot to put the item in
	const auto ToSlotIndex = FindFirstStackableIndex(ItemStack->GetItem());

	// split the incoming item stack by the amount to transfer
	auto NewItemStack = ItemStack->Split(Amount);

	// there is no existing slot that we can stack onto
	if (!IsValidIndex(ToSlotIndex)) {

		// append the new stack onto the end of the contents
		const auto NewSlotIndex = Contents.Add(NewItemStack);

		// save the slot index where the new stack was added
		OutResult.SlotIndex = NewSlotIndex;

		// add a lambda for when the stack amount changes
		// todo: maybe this shouldn't be inline?
		NewItemStack->OnItemStackAmountChangedNative.AddLambda(
			[this, NewSlotIndex](UItemStack* ItemStack, int32 AmountDifference) {

				FItemContainerSlotChangeInfo SlotChangeInfo;

				if (ItemStack->GetAmount() < 1) {
					// todo: remove the item

				}

				if (AmountDifference > 0) {
					SlotChangeInfo.Type = EItemContainerSlotChangeType::Item_Added;
					SlotChangeInfo.SourceContainer = nullptr;
					SlotChangeInfo.SourceSlotIndex = INDEX_NONE;
					SlotChangeInfo.DestinationContainer = this;
					SlotChangeInfo.DestinationSlotIndex = NewSlotIndex;
				} else {
					SlotChangeInfo.Type = EItemContainerSlotChangeType::Item_Removed;
					SlotChangeInfo.SourceContainer = this;
					SlotChangeInfo.SourceSlotIndex = NewSlotIndex;
					SlotChangeInfo.DestinationContainer = nullptr;
					SlotChangeInfo.DestinationSlotIndex = INDEX_NONE;
				}

				TotalItemAmount += AmountDifference;

				NotifyContainerSlotChanged(SlotChangeInfo);
			});

	} else {
		// add to existing stack
		Contents[ToSlotIndex]->SetAmount(Contents[ToSlotIndex]->GetAmount() + Amount);
		OutResult.SlotIndex = ToSlotIndex;
	}

	TotalItemAmount += Amount;
	OutResult.AmountTransferred = Amount;

	return true;
}


bool UItemContainer::TransferItem(
	struct FItemContainerItemTransferResult& OutResult,
	const int FromSlotIndex,
	UItemContainer* ToContainer,
	const int Amount,
	const bool AllowPartial
) {
	// invalid parameters
	if (Amount < INDEX_NONE || Amount == 0)
		return false;

	// invalid container
	if (!IsValid(ToContainer))
		return false;

	// get item at the slot index
	UItemStack* ItemStack;
	if (!TryGetItemStackAtIndex(FromSlotIndex, ItemStack))
		// no item in the slot
		return false;

	// container can not accept the item
	if (!ToContainer->CanAcceptItem(ItemStack->GetItem()))
		return false;

	auto AmountToTransfer = Amount == INDEX_NONE ? ItemStack->GetAmount() : FMath::Min(ItemStack->GetAmount(), Amount);
	const auto Remaining = ToContainer->RemainingCapacity();

	if (AmountToTransfer > Remaining) {

		if (!AllowPartial)
			return false;

		// we need to split the stack
		OutResult.TransferredPartial = true;
		AmountToTransfer = Remaining;
	}

	if (!ToContainer->AddItemStack_Internal(OutResult, ItemStack, AmountToTransfer))
		return false;

	FItemContainerSlotChangeInfo ChangeInfo;

	ChangeInfo.Type = EItemContainerSlotChangeType::Item_Transfer_Sent;
	ChangeInfo.SourceContainer = this;
	ChangeInfo.SourceSlotIndex = FromSlotIndex;
	ChangeInfo.DestinationContainer = ToContainer;
	ChangeInfo.DestinationSlotIndex = OutResult.SlotIndex;

	NotifyContainerSlotChanged(ChangeInfo);

	return true;


}

void UItemContainer::NotifyContainerSlotChanged(const FItemContainerSlotChangeInfo Info) {

	// broadcast to native first
	OnContainerSlotChangedNative.Broadcast(Info);
	OnContainerSlotChanged.Broadcast(Info);

	// call blueprint event
	ContainerSlotChanged(Info);

}
