#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "../ChimeraAssetManager.h"
#include "ResourceItem.generated.h"

/**
 * Resource Item
 */
UCLASS()
class CHIMERA_API UResourceItem : public UItem {

	GENERATED_BODY()

public:

	UResourceItem() {
		Type = UChimeraAssetManager::ResourceItemType;
		IsStackable = true;
		IsDamageable = false;
	}

	// todo: have a way to link this the voxel materials
};
