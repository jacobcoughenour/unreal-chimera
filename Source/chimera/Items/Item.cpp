// Fill out your copyright notice in the Description page of Project Settings.

#include "Item.h"

#include "DrawDebugHelpers.h"
#include "ItemStack.h"
#include "chimera/chimera.h"
#include "Kismet/KismetSystemLibrary.h"


FString UItem::GetIdentifierString() const {
	return GetPrimaryAssetId().ToString();
}

FPrimaryAssetId UItem::GetPrimaryAssetId() const {
	return FPrimaryAssetId(Type, GetFName());
}

bool UItem::TraceInvokerView_Implementation(FHitResult& HitResult, AActor* InvokingActor, const float Distance) {
	if (!InvokingActor->GetClass()->ImplementsInterface(UItemInvokingActor::StaticClass()))
		return false;

	const auto Ray = IItemInvokingActor::Execute_GetViewRay(InvokingActor);

	FCollisionQueryParams Params;
	Params.AddIgnoredActor(InvokingActor);

	bool Hit = InvokingActor->GetWorld()->LineTraceSingleByChannel(
		HitResult,
		Ray.v1,
		Ray.v1 + (Ray.v2 * Distance),
		ECC_Visibility,
		Params
	);

	// // if (Hit) {
	// DrawDebugLine(InvokingActor->GetWorld(), Ray.v1,
	// 			Ray.v1 + (Ray.v2 * Distance), FColor::Magenta, true);
	// DrawDebugPoint(InvokingActor->GetWorld(),
	// 				HitResult.Location, 5, FColor::Red, true);
	// // }

	return Hit;
}

bool UItem::IsDamaged_Implementation(UItemStack* ItemStack) {
	return IsDamageable && GetDamage(ItemStack) > 0;
}

bool UItem::IsBroken_Implementation(UItemStack* ItemStack) {
	return IsDamageable && GetDamage(ItemStack) >= GetMaxDamage(ItemStack);
}

int32 UItem::GetDamage_Implementation(UItemStack* ItemStack) {
	return 0;
}

void UItem::SetDamage_Implementation(UItemStack* ItemStack, const int InDamage) { }

void UItem::TakeDamage_Implementation(UItemStack* ItemStack, const int InDamage) {
	if (InDamage < 0)
		return;
	SetDamage(ItemStack, GetDamage(ItemStack) + InDamage);
}


int32 UItem::GetMaxDamage_Implementation(UItemStack* ItemStack) {
	return 0;
}

FBlueprintJsonObject UItem::GetDefaultItemData_Implementation() {
	// create an empty json object
	return UBlueprintJsonLibrary::JsonMake();
}

void UItem::OnInitializeItemData_Implementation(FBlueprintJsonObject& ItemData) {
	// assign the input values to the defaults
	ItemData = UBlueprintJsonLibrary::JsonAssign(GetDefaultItemData(), ItemData);
}

void UItem::OnConstruction_Implementation(UItemStack* ItemStack) { }
void UItem::OnItemDataChanged_Implementation(UItemStack* ItemStack) { }
void UItem::OnActivation_Implementation(AActor* InvokingActor, UItemStack* ItemStack) {}

void UItem::OnDeactivation_Implementation(AActor* InvokingActor, UItemStack* ItemStack) {
	InvokingActor->GetWorldTimerManager().ClearTimer(ItemStack->GetItemPrimaryActionTimerHandle());
	InvokingActor->GetWorldTimerManager().ClearTimer(ItemStack->GetItemSecondaryActionTimerHandle());
}

void UItem::Tick_Implementation(AActor* InvokingActor, UItemStack* ItemStack) { }

void UItem::DoPrimaryAction_Implementation(AActor* InvokingActor, UItemStack* ItemStack, FHitResult HitResult) {}
void UItem::DoSecondaryAction_Implementation(AActor* InvokingActor, UItemStack* ItemStack, FHitResult HitResult) {}

void UItem::OnPrimaryActionDown_Implementation(AActor* InvokingActor, UItemStack* ItemStack) {
	if (IsBroken(ItemStack))
		return;

	InvokingActor->GetWorldTimerManager().ClearTimer(ItemStack->GetItemPrimaryActionTimerHandle());

	FTimerDelegate ActionDelegate;
	ActionDelegate.BindLambda([this, InvokingActor, ItemStack] {
		FHitResult HitResult;
		if (TraceInvokerView(HitResult, InvokingActor)) {
			DoPrimaryAction(InvokingActor, ItemStack, HitResult);
		}
	});

	if (PrimaryActionFireRate <= 0.0f && PrimaryActionInitialDelay <= 0.0f) {
		ActionDelegate.Execute();
	} else {

		if (PrimaryActionFireRate > 0.0f) {
			InvokingActor->GetWorldTimerManager().SetTimer(
				ItemStack->GetItemPrimaryActionTimerHandle(),
				ActionDelegate,
				PrimaryActionFireRate,
				true,
				PrimaryActionInitialDelay <= 0.0f ? -1.0f : PrimaryActionInitialDelay
			);
		} else {
			InvokingActor->GetWorldTimerManager().SetTimer(
				ItemStack->GetItemPrimaryActionTimerHandle(),
				ActionDelegate,
				PrimaryActionInitialDelay,
				false
			);
		}

	}


}

void UItem::OnPrimaryActionUp_Implementation(AActor* InvokingActor, UItemStack* ItemStack) {
	InvokingActor->GetWorldTimerManager().ClearTimer(ItemStack->GetItemPrimaryActionTimerHandle());
}

void UItem::OnSecondaryActionDown_Implementation(AActor* InvokingActor, UItemStack* ItemStack) {
	if (IsBroken(ItemStack))
		return;

	InvokingActor->GetWorldTimerManager().ClearTimer(ItemStack->GetItemSecondaryActionTimerHandle());

	FTimerDelegate ActionDelegate;
	ActionDelegate.BindLambda([this, InvokingActor, ItemStack] {
		FHitResult HitResult;
		if (TraceInvokerView(HitResult, InvokingActor)) {
			DoSecondaryAction(InvokingActor, ItemStack, HitResult);
		}
	});

	if (SecondaryActionFireRate <= 0.0f && SecondaryActionInitialDelay <= 0.0f) {
		ActionDelegate.Execute();
	} else {

		if (SecondaryActionFireRate > 0.0f) {
			InvokingActor->GetWorldTimerManager().SetTimer(
				ItemStack->GetItemSecondaryActionTimerHandle(),
				ActionDelegate,
				SecondaryActionFireRate,
				true,
				SecondaryActionInitialDelay <= 0.0f ? -1.0f : SecondaryActionInitialDelay
			);
		} else {
			InvokingActor->GetWorldTimerManager().SetTimer(
				ItemStack->GetItemSecondaryActionTimerHandle(),
				ActionDelegate,
				SecondaryActionInitialDelay,
				false
			);
		}

	}
}

void UItem::OnSecondaryActionUp_Implementation(AActor* InvokingActor, UItemStack* ItemStack) {
	InvokingActor->GetWorldTimerManager().ClearTimer(ItemStack->GetItemSecondaryActionTimerHandle());
}
