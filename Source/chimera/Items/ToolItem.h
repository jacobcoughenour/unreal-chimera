#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "ItemStack.h"
#include "../ChimeraAssetManager.h"

#include "ToolItem.generated.h"

/**
 * Tool Item
 */
UCLASS()
class CHIMERA_API UToolItem : public UItem {

	GENERATED_BODY()

public:

	UToolItem() {
		Type = UChimeraAssetManager::ToolItemType;
		IsStackable = false;
		IsDamageable = true;
	}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Tool)
	int32 DefaultMaxDamage = 100;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Tool)
	float HitRange = 320.0f;

	virtual bool
	TraceInvokerView_Implementation(FHitResult& HitResult, AActor* InvokingActor, const float Distance) override {
		return Super::TraceInvokerView_Implementation(HitResult, InvokingActor, HitRange);
	};

	virtual FBlueprintJsonObject GetDefaultItemData_Implementation() override {
		auto Object = Super::GetDefaultItemData_Implementation();

		Object = UBlueprintJsonLibrary::JsonMakeField(
			Object,
			FString(TEXT("Damage")),
			UBlueprintJsonLibrary::JsonMakeInt(0)
		);

		Object = UBlueprintJsonLibrary::JsonMakeField(
			Object,
			FString(TEXT("MaxDamage")),
			UBlueprintJsonLibrary::JsonMakeInt(DefaultMaxDamage)
		);

		return Object;
	}

	virtual int32 GetDamage_Implementation(UItemStack* ItemStack) override {
		int32 Value = 0;
		if (ItemStack->GetItemData().Object.IsValid())
			ItemStack->GetItemData().Object->TryGetNumberField(FString(TEXT("Damage")), Value);
		return Value;
	}

	virtual int32 GetMaxDamage_Implementation(UItemStack* ItemStack) override {
		int32 Value = 0;
		if (ItemStack->GetItemData().Object.IsValid())
			ItemStack->GetItemData().Object->TryGetNumberField(FString(TEXT("MaxDamage")), Value);
		return Value;
	}

	virtual void SetDamage_Implementation(UItemStack* ItemStack, const int InDamage) override {
		if (InDamage < 0 || InDamage > GetMaxDamage(ItemStack))
			return;

		auto Data = ItemStack->GetItemData();

		if (Data.Object.IsValid()) {
			ItemStack->SetItemData(UBlueprintJsonLibrary::JsonMakeField(
					Data,
					FString(TEXT("Damage")),
					UBlueprintJsonLibrary::JsonMakeInt(InDamage)
				)
			);
		}
	}

};
