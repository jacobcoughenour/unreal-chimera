// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/AssetManager.h"
#include "Items/Item.h"

#include "ChimeraAssetManager.generated.h"

/**
 * Custom Asset Manager for Chimera
 */
UCLASS()
class CHIMERA_API UChimeraAssetManager : public UAssetManager {
	GENERATED_BODY()

public:

	UChimeraAssetManager() {}

	// DEFINE NEW ITEM TYPES HERE
	//
	// you also need to:
	//     - set the TEXT value for the asset type in the .cpp of this file
	//     - create a subclass of UItem for your new type
	//     - add a new entry to Project Settings -> Asset Manager -> Primary Asset Types to Scan

	static const FPrimaryAssetType ResourceItemType;
	static const FPrimaryAssetType ToolItemType;

	/**
	 * @return Instance of the ChimeraAssetManager
	 */
	static UChimeraAssetManager& GetInstance();

	/**
	 * Synchronously loads the Item Definition asset but can cause hitches.
	 * @param PrimaryAssetId Asset ID of the item definition
	 * @param bLogWarning log a warning if the asset fails to load
	 * @return the loaded item definition
	 */
	UItem* ForceLoadItem(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning = true);

};
