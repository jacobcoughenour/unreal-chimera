// Fill out your copyright notice in the Description page of Project Settings.

#include "Planet.h"


#include "ChimeraGameInstance.h"
#include "DrawDebugHelpers.h"
#include "IVoxelPool.h"
#include "VoxelDebug/VoxelDebugUtilities.h"
#include "VoxelTools/VoxelDataTools.h"
#include "VoxelTools/VoxelProjectionTools.h"
#include "VoxelWorld.h"

// Sets default values
APlanet::APlanet() {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("PlanetRoot"));

	DirectionalLightComponent = CreateDefaultSubobject<UDirectionalLightComponent>(TEXT("SunLight"));
	DirectionalLightComponent->SetupAttachment(RootComponent);

	SkyAtmosphereComponent = CreateDefaultSubobject<USkyAtmosphereComponent>(TEXT("PlanetSkyAtmosphere"));
	SkyAtmosphereComponent->SetupAttachment(RootComponent);

	SkyLightComponent = CreateDefaultSubobject<USkyLightComponent>(TEXT("PlanetSkyLight"));
	SkyLightComponent->SetupAttachment(RootComponent);
}

void APlanet::OnConstruction(const FTransform& Transform) {
	Super::OnConstruction(Transform);

	// position the sun light
	DirectionalLightComponent->bUsedAsAtmosphereSunLight = true;

	// match the atmosphere to match the planet generator settings
	SkyAtmosphereComponent->TransformMode = ESkyAtmosphereTransformMode::PlanetCenterAtComponentTransform;
	SkyAtmosphereComponent->BottomRadius = GeneratorSeaLevel * VoxelSize * 0.00001f;
	SkyAtmosphereComponent->AtmosphereHeight = GeneratorTerrainHeight * 16.0f * VoxelSize * 0.00001f;

	SkyLightComponent->SetMobility(EComponentMobility::Movable);
	SkyLightComponent->SourceType = ESkyLightSourceType::SLS_CapturedScene;
	// SkyLightComponent->SkyDistanceThreshold = (GeneratorSeaLevel + GeneratorTerrainHeight) * VoxelSize;
}

void APlanet::OnPlanetLayerCreated_Implementation(const PlanetLayerType& LayerType, AVoxelWorld* LayerWorld) {

	// todo bind chunk events here (activate, deactivate, generate)

}

void APlanet::OnPlanetLayerSpawned_Implementation(const PlanetLayerType& LayerType, AVoxelWorld* LayerWorld) { }


void APlanet::OnVoxelChunkActivate_Implementation(const PlanetLayerType& LayerType, AVoxelWorld* LayerWorld,
												const FVoxelIntBox& Bounds) {}

void APlanet::OnVoxelChunkDeactivate_Implementation(const PlanetLayerType& LayerType, AVoxelWorld* LayerWorld,
													const FVoxelIntBox& Bounds) {}


void APlanet::OnVoxelChunkGenerate_Implementation(const PlanetLayerType& LayerType, AVoxelWorld* LayerWorld,
												const FVoxelIntBox& Bounds) {


	// todo this whole random tree thing should be generalized for use with more foliage types

	// only generate on solid layer
	if (LayerType != PlanetLayerType::PLANET_LAYER_SOLID)
		return;

	if (!IsValid(LayerWorld))
		return;

	// todo this aborts any chunk that touches the sealevel.  maybe it should only abort if the chunk is entirely under the sea level?
	// todo so really we just need a better way to avoid the sea level
	uint64 ClosestDistanceSquared = Bounds.ComputeSquaredDistanceFromBoxToPoint(LayerWorld->GetWorldOffset());
	uint64 SeaLevelDistanceSquared = pow<uint64>(GeneratorSeaLevel, 2);

	if (SeaLevelDistanceSquared >= ClosestDistanceSquared)
		return;


	// get world data pointer
	auto& WorldData = LayerWorld->GetData();

	// if value range in chunk is empty
	// this really depends on the accuracy of GetValueRangeImpl() in the generator
	if (WorldData.IsEmpty(Bounds, 0))
		return;

	// generate line

	// const auto GlobalBounds = UVoxelBlueprintLibrary::TransformVoxelBoxToGlobalBox(LayerWorld, Bounds);

	// const auto GlobalCenter = GlobalBounds.GetCenter();
	// const auto GlobalExtent = GlobalBounds.GetExtent().Size();
	const FIntVector Size = Bounds.Size();

	const auto SamplePoint = Bounds.GetCenter();
	const auto A = FoliageNoise.GetValue_3D(SamplePoint.X, SamplePoint.Y, SamplePoint.Z, 0.1f);
	const auto B = FoliageNoise.GetValue_3D(A * 100.f, SamplePoint.Y, SamplePoint.Z, 0.953f);
	const auto C = FoliageNoise.GetValue_3D(SamplePoint.X, SamplePoint.Y, B * 100.f, 0.53f);

	// const auto A = 0.0f;
	// const auto B = 0.0f;
	// const auto C = 0.0f;

	// random point within a sphere within the bounds
	const auto Point = Bounds.GetCenter() + FVector(A, B, C) * Size.X * 0.5f;

	// the normal between our random point and the center of the planet
	// (the "up" direction)
	const FVector Direction = (Point - LayerWorld->GetWorldOffset()).GetSafeNormal().ToFloat();

	// line trace start and end points clamped to the edges of the box
	const FVector From = (Point + Direction * Size.X).ToFloat()
		.BoundToBox(FVector(Bounds.Min), FVector(Bounds.Max));
	const FVector To = (Point - Direction * Size.X).ToFloat()
		.BoundToBox(FVector(Bounds.Min), FVector(Bounds.Max));

	// DrawDebugLine(GetWorld(), LayerWorld->LocalToGlobalFloat(From), LayerWorld->LocalToGlobalFloat(To), FColor::Cyan,
	// false, 20.f);

	FVoxelHitResult Result;

	if (LineTraceVoxelsByPredicate(Result, LayerWorld, From, To, [](const float& Density) -> bool {
		// only hit the voxels close to the surface
		return Density < 0.0f && Density > -0.2f;
	})) {

		// DrawDebugLine(GetWorld(), LayerWorld->LocalToGlobalFloat(From), LayerWorld->LocalToGlobalFloat(To),
		// 			FColor::Blue, true);
		//
		// DrawDebugPoint(GetWorld(), LayerWorld->LocalToGlobal(Result.VoxelPosition), 10.f, FColor::Red, true);
		// DrawDebugPoint(GetWorld(), LayerWorld->LocalToGlobalFloat(Result.ImpactPoint), 10.f, FColor::Blue, true);

		// UVoxelDebugUtilities::DrawDebugIntBox(
		// LayerWorld, FVoxelIntBox(Result.VoxelPosition), 1000, 1.f,
		// FColor::White);

		SpawnTree(LayerWorld, Result.VoxelPosition, Direction);
	}

}

void APlanet::SpawnTree_Implementation(AVoxelWorld* LayerWorld, const FIntVector& VoxelPoint,
										const FVector& SurfaceNormal) {}

bool APlanet::LineTraceVoxels(FVoxelHitResult& OutHit, AVoxelWorld* LayerWorld, const FVector& Start,
							const FVector& End) const {
	return LineTraceVoxelsByPredicate(OutHit, LayerWorld, Start, End, [](const float& Density) -> bool {
		return Density < 0.0f;
	});
}

template <typename UnaryPredicate>
bool APlanet::LineTraceVoxelsByPredicate(FVoxelHitResult& OutHit, AVoxelWorld* LayerWorld, const FVector& Start,
										const FVector& End, const UnaryPredicate& Pred) const {

	// http://www.cse.yorku.ca/~amana/research/grid.pdf
	// https://stackoverflow.com/questions/12367071/how-do-i-initialize-the-t-variables-in-a-fast-voxel-traversal-algorithm-for-ray#12370474

	// avoid vectors cause they seem to slow this down

	const int Dx = FMath::Sign(End.X - Start.X);
	const int Dy = FMath::Sign(End.Y - Start.Y);
	const int Dz = FMath::Sign(End.Z - Start.Z);

	const float MaxDelta = 100000.f;

	const float DeltaX = Dx == 0 ? MaxDelta : FMath::Min(Dx / (End.X - Start.X), MaxDelta);
	const float DeltaY = Dy == 0 ? MaxDelta : FMath::Min(Dy / (End.Y - Start.Y), MaxDelta);
	const float DeltaZ = Dz == 0 ? MaxDelta : FMath::Min(Dz / (End.Z - Start.Z), MaxDelta);

	float MaxX = (Dx > 0 ? 1 - Start.X + FMath::FloorToFloat(Start.X) : Start.X - FMath::FloorToFloat(Start.X)) *
				DeltaX;
	float MaxY = (Dy > 0 ? 1 - Start.Y + FMath::FloorToFloat(Start.Y) : Start.Y - FMath::FloorToFloat(Start.Y)) *
				DeltaY;
	float MaxZ = (Dz > 0 ? 1 - Start.Z + FMath::FloorToFloat(Start.Z) : Start.Z - FMath::FloorToFloat(Start.Z)) *
				DeltaZ;

	// the current voxel position for the loop
	FIntVector CurrentVoxel = FIntVector(
		FMath::FloorToInt(Start.X),
		FMath::FloorToInt(Start.Y),
		FMath::FloorToInt(Start.Z)
	);

	while (true) {

		// move the voxel position
		if (MaxX < MaxY) {
			if (MaxX < MaxZ) {
				CurrentVoxel.X += Dx;
				MaxX += DeltaX;
			} else {
				CurrentVoxel.Z += Dz;
				MaxZ += DeltaZ;
			}
		} else {
			if (MaxY < MaxZ) {
				CurrentVoxel.Y += Dy;
				MaxY += DeltaY;
			} else {
				CurrentVoxel.Z += Dz;
				MaxZ += DeltaZ;
			}
		}

		// we reached the end without a hit
		if (MaxX > 1 && MaxY > 1 && MaxZ > 1) return false;

		// get density at the current voxel
		float Density = 0.0f;
		UVoxelDataTools::GetValue(Density, LayerWorld, CurrentVoxel);

		// if we are under the density threshold for a hit
		if (Pred(Density)) {

			// hit
			OutHit.VoxelPosition = CurrentVoxel;
			OutHit.VoxelDensity = Density;
			OutHit.ImpactPoint = FVector(CurrentVoxel.X - MaxX, CurrentVoxel.Y - MaxY, CurrentVoxel.Z - MaxZ);

			return true;
		}
	}

}

// Called when the game starts or when spawned
void APlanet::BeginPlay() {
	Super::BeginPlay();

	FoliageNoise.SetSeed(GeneratorNoiseSeed);

	for (int i = 0; i < 2; i++) {

		PlanetLayerType Type = i == 0 ? PlanetLayerType::PLANET_LAYER_SOLID : PlanetLayerType::PLANET_LAYER_LIQUID;

		auto Layer = GetWorld()->SpawnActor<AVoxelWorld>();

		FString Name = Type == PlanetLayerType::PLANET_LAYER_SOLID ? TEXT("Solid Layer") : TEXT("Liquid Layer");
		Layer->Rename(*Name);

#if WITH_EDITOR
		Layer->SetActorLabel(Name);
#endif

		Layer->SetCanBeDamaged(false);
		// layer->SetReplicates(true);

		Layer->bCreateGlobalPool = !IVoxelPool::GetPoolForWorld(GetWorld()).IsValid();
		Layer->bCreateWorldAutomatically = false;
		Layer->bCastFarShadow = true;
		Layer->bGenerateDistanceFields = true;

		Layer->bDisableOnScreenMessages = bDisableOnScreenMessages;

		Layer->VoxelSize = VoxelSize;
		Layer->SetWorldSize((GeneratorSeaLevel + GeneratorTerrainHeight) * 2);

		if (Type == PlanetLayerType::PLANET_LAYER_LIQUID) {
			Layer->CollisionPresets.SetCollisionProfileName(TEXT("VoxelLiquidLayer"));
			Layer->bGenerateOverlapEvents = true;

			Layer->MaterialConfig = EVoxelMaterialConfig::SingleIndex;
			Layer->MaterialCollection = MaterialCollectionLiquid;
		} else {
			Layer->CollisionPresets.SetCollisionProfileName(TEXT("VoxelSolidLayer"));

			Layer->MaterialConfig = EVoxelMaterialConfig::MultiIndex;
			Layer->MaterialCollection = MaterialCollection;
		}

		Layer->Seeds.Add("Layer Index", i);
		Layer->Seeds.Add("Noise Seed", GeneratorNoiseSeed);
		Layer->Seeds.Add("Noise Frequency", GeneratorNoiseFrequency);
		Layer->Seeds.Add("Secondary Noise Frequency", GeneratorSecondaryNoiseFrequency);
		Layer->Seeds.Add("Noise Exponent", GeneratorNoiseExponent);
		Layer->Seeds.Add("Noise Octaves", GeneratorNoiseOctaves);
		Layer->Seeds.Add("Secondary Noise Octaves", GeneratorSecondaryNoiseOctaves);
		Layer->Seeds.Add("Sea Level", GeneratorSeaLevel);
		Layer->Seeds.Add("Terrain Height", GeneratorTerrainHeight);
		Layer->Seeds.Add("Secondary Terrain Height", GeneratorSecondaryTerrainHeight);

		Layer->WorldGenerator = PlanetGenerator;

		Layer->AttachToComponent(RootComponent, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false), "");

		PlanetLayerActors[i] = Layer;

		OnPlanetLayerSpawned(Type, Layer);

		Layer->CreateWorld();

		OnPlanetLayerCreated(Type, Layer);
	}

	// recapture the sky lighting
	// SkyLightComponent->RecaptureSky();

	UChimeraGameInstance::GetChimeraGameInstance(this)->RegisterPlanet(this);

}

void APlanet::DestroyLayers() {
	for (int i = 0; i < 2; i++) {
		if (IsValid(PlanetLayerActors[i])) {
			if (PlanetLayerActors[i]->IsCreated())
				PlanetLayerActors[i]->DestroyWorld();
			PlanetLayerActors[i]->Destroy();
			PlanetLayerActors[i] = nullptr;
		}
	}
}

void APlanet::EndPlay(const EEndPlayReason::Type EndPlayReason) {
	Super::EndPlay(EndPlayReason);

	UChimeraGameInstance::GetChimeraGameInstance(this)->UnregisterPlanet(this);

	DestroyLayers();
}

// Called every frame
void APlanet::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

FVector APlanet::GetPlanetaryGravityAtPoint(const UObject* WorldContextObject, const FVector Point) {

	FVector Resultant(0, 0, 0);

	const auto GameInstance = UChimeraGameInstance::GetChimeraGameInstance(WorldContextObject);

	for (auto& Planet : GameInstance->RegisteredPlanets) {

		// vector pointing from point to planet
		const FVector Diff = Planet->GetActorLocation() - Point;

		// distance between the two objects
		const float Distance = Diff.Size();

		if (Distance < SMALL_NUMBER)
			continue;

		const float PlanetRadius = Planet->GeneratorSeaLevel * Planet->VoxelSize;
		const float Ratio = Distance / PlanetRadius;

		// continue if point is 10x the radius away
		if (Ratio > 4.f) {
			continue;
		}

		const float Force = Ratio > 1.0f
								// external gravity
								? Planet->SeaLevelGravity / FMath::Square(Ratio)
								// internal gravity
								: ((1.f - FMath::Square(1.f - Ratio)) * Planet->SeaLevelGravity);

		Resultant += Diff * (Force / Distance);

	}

	return Resultant;
}
