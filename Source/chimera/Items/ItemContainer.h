﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../chimera.h"
#include "CoreMinimal.h"
#include "Item.h"
#include "ItemStack.h"
#include "UObject/NoExportTypes.h"
#include "ItemContainer.generated.h"

class UItemContainer;

USTRUCT(BlueprintType)
struct FItemContainerItemTransferResult {
	GENERATED_BODY()

	/**
	 * Slot Index where the item was transferred to.
	 *
	 * INDEX_NONE if no amount of the item was transferred.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 SlotIndex = INDEX_NONE;


	/**
	 * Amount of the item that was transferred.
	 *
	 * 0 if none of the item was transferred.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 AmountTransferred = 0;

	/**
	 * true if AllowPartial is true and a partial amount was transferred.
	 * false if AllowPartial is true if the full amount was transferred.
	 * false if AllowPartial is false and no amount of the item was transferred.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool TransferredPartial = false;

};

UENUM(BlueprintType)
enum class EItemContainerSlotChangeType: uint8 {
	/**
	 * Nothing changed
	 */
	None = 0,
	/**
	 * Item was moved to a different slot in this container.
	 */
	Item_Moved = 1,
	/**
	 * Item was added to this container (not from another container)
	 */
	Item_Added = 2,
	/**
	 * Item was removed from this container (and did not move to another container)
	 */
	Item_Removed = 3,
	/**
	 * Item was transferred from this container to another.
	 */
	Item_Transfer_Sent = 4,
	/**
	 * Item was transferred from another container to this one.
	 */
	Item_Transfer_Received = 5,
	/**
	 * Total count of event types
	 */
	Count UMETA(Hidden)
};

USTRUCT(BlueprintType)
struct FItemContainerSlotChangeInfo {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EItemContainerSlotChangeType Type = EItemContainerSlotChangeType::None;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UItemContainer* SourceContainer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 SourceSlotIndex = INDEX_NONE;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UItemContainer* DestinationContainer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 DestinationSlotIndex = INDEX_NONE;

};


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnContainerSlotChanged, struct FItemContainerSlotChangeInfo, Info);

DECLARE_MULTICAST_DELEGATE_OneParam(FOnContainerSlotChangedNative, struct FItemContainerSlotChangeInfo);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnContainerLoaded);

DECLARE_MULTICAST_DELEGATE(FOnContainerLoadedNative);

/**
 * Item Container
 */
UCLASS(Blueprintable, BlueprintType, EditInlineNew, meta=(DontUseGenericSpawnObject))
class CHIMERA_API UItemContainer : public UObject {

	GENERATED_BODY()

protected:

	UPROPERTY(Instanced)
	TArray<UItemStack*> Contents;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 Capacity;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 TotalItemAmount;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FText PanelName = NSLOCTEXT("Chimera", "ItemContainer", "Item Container");

public:

	UItemContainer() { };

	UFUNCTION(BlueprintCallable)
	static UItemContainer* CreateItemContainer(
		const TSubclassOf<UItemContainer> ItemContainerClass,
		UObject* Outer,
		FText InitialPanelName,
		TArray<UItemStack*> InitialContents,
		int32 InitialCapacity = 1
	) {
		auto Container = NewObject<UItemContainer>(Outer, ItemContainerClass);

		Container->PanelName = InitialPanelName;
		// Container->Contents = InitialContents;
		Container->Capacity = InitialCapacity;
		// Container->TotalItemAmount = GetItemCountInArray(InitialContents);

		for (auto ItemStack : InitialContents) {
			if (ItemStack != nullptr) {
				FItemContainerItemTransferResult Result;
				Container->AddItemStack(Result, ItemStack, false);
			}
		}

		if (Container->TotalItemAmount > Container->Capacity) {
			UE_LOG(LogChimera, Warning, TEXT("ItemContainer %s has %d items when its capacity is %d"),
					*GetNameSafe(Container),
					Container->TotalItemAmount, Container->Capacity);
		}

		return Container;
	}

	static int32 GetItemCountInArray(TArray<UItemStack*> Items);

	UFUNCTION(BlueprintCallable)
	void SetPanelName(FText NewPanelName);

	UFUNCTION(BlueprintCallable)
	bool SetCapacity(int32 NewCapacity);

	UFUNCTION(BlueprintPure)
	int32 ItemCount() const;

	UFUNCTION(BlueprintPure)
	bool IsFull() const;

	UFUNCTION(BlueprintPure)
	int32 RemainingCapacity() const;

	UFUNCTION(BlueprintPure)
	bool CanAcceptItem(
		const UItem* Item
	) const;

	UFUNCTION(BlueprintPure)
	bool IsValidIndex(const int32 SlotIndex) const;

	/**
	 * this is kinda unsafe so use TryGetItemStackAtIndex()
	 */
	// UFUNCTION(BlueprintCallable, BlueprintPure)
	UItemStack* GetItemStackAtIndex(const int32 SlotIndex) const;

	UFUNCTION(BlueprintPure)
	bool TryGetItemStackAtIndex(const int32 SlotIndex, UItemStack*& OutItemStack);

	UFUNCTION(BlueprintPure)
	int32 FindFirstStackableIndex(const UItem* Item) const;

	UFUNCTION(BlueprintCallable)
	bool AddItemStack(
		struct FItemContainerItemTransferResult& OutResult,
		UItemStack* ItemStack,
		const bool AllowPartial = false
	);

protected:

	bool AddItemStack_Internal(
		struct FItemContainerItemTransferResult& OutResult,
		UItemStack* ItemStack,
		const int Amount
	);

public:

	// UFUNCTION(BlueprintCallable)
	// bool RemoveItemAmount(
	// 	struct FItemContainerItemTransferResult& OutResult,
	// 	const int FromSlotIndex,
	// 	const int Amount = 1,
	// 	const bool AllowPartial = false
	// );

	UFUNCTION(BlueprintCallable)
	bool TransferItem(
		struct FItemContainerItemTransferResult& OutResult,
		const int FromSlotIndex,
		UItemContainer* ToContainer,
		const int Amount = -1,
		const bool AllowPartial = false
	);


protected:

	void NotifyContainerSlotChanged(struct FItemContainerSlotChangeInfo Info);

public:

	UFUNCTION(BlueprintImplementableEvent)
	void ContainerSlotChanged(struct FItemContainerSlotChangeInfo Info);

	/**
	 * Delegate called when a slot is modified
	 */
	UPROPERTY(BlueprintAssignable)
	FOnContainerSlotChanged OnContainerSlotChanged;

	FOnContainerSlotChangedNative OnContainerSlotChangedNative;

};
