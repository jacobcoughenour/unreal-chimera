#include "PlanetGenerator.h"

#include "VoxelMaterialBuilder.h"

TMap<FName, int32> UPlanetGenerator::GetDefaultSeeds() const {
	TMap<FName, int32> DefaultMap;
	return DefaultMap;
}

TVoxelSharedRef<FVoxelWorldGeneratorInstance> UPlanetGenerator::GetInstance() {
	return MakeVoxelShared<FVoxelPlanetGeneratorInstance>(*this);
}

FVoxelPlanetGeneratorInstance::FVoxelPlanetGeneratorInstance(const UPlanetGenerator& PlanetGenerator) {}

void FVoxelPlanetGeneratorInstance::Init(const FVoxelWorldGeneratorInit& InitStruct) {
	Noise.SetSeed(InitStruct.Seeds["Noise Seed"]);
	LayerIndex = InitStruct.Seeds["Layer Index"];

	NoiseFrequency = InitStruct.Seeds["Noise Frequency"];
	SecondaryNoiseFrequency = InitStruct.Seeds["Secondary Noise Frequency"];

	NoiseExponent = InitStruct.Seeds["Noise Exponent"];

	NoiseOctaves = InitStruct.Seeds["Noise Octaves"];
	SecondaryNoiseOctaves = InitStruct.Seeds["Secondary Noise Octaves"];

	SeaLevel = InitStruct.Seeds["Sea Level"];
	TerrainHeight = InitStruct.Seeds["Terrain Height"];
	SecondaryTerrainHeight = InitStruct.Seeds["Secondary Terrain Height"];

}

v_flt FVoxelPlanetGeneratorInstance::GetValueImpl(v_flt X, v_flt Y, v_flt Z, int32 LOD,
												const FVoxelItemStack& Items) const {

	// TODO: get planet center from generator settings
	const FIntVector PlanetCenter = FIntVector(0, 0, 0);

	const FVector SamplePosition = FVector(X, Y, Z).GetSafeNormal();

	float Height = SeaLevel;

	float dist = FVector(X, Y, Z).Size();

	float n = Noise.GetSimplexFractal_3D(
		SamplePosition.X,
		SamplePosition.Y,
		SamplePosition.Z,
		NoiseFrequency, NoiseOctaves
	);

	// height map noise
	Height += ((n < 0.f ? -1.f : 1.f) * abs(pow(n, NoiseExponent)))
		* TerrainHeight;

	// positive = empty
	// negative = full
	float Sample = dist - Height;

	// offset terrain to create overhangs
	Sample += Noise.GetSimplexFractal_3D(
		X,
		Y,
		Z,
		1.f / SecondaryNoiseFrequency, SecondaryNoiseOctaves
	) * SecondaryTerrainHeight;


	if (LayerIndex == static_cast<int>(PlanetLayerType::PLANET_LAYER_LIQUID)) {

		Sample = Sample > -5.0f
					?
					// ocean sea level
					dist - SeaLevel
					// part where the water meets the terrain
					: -Sample - 5.0f;
	} else {

		// TODO cave generation

		// inner core
		if (dist < 65.f) {
			Sample = FMath::Clamp(dist - 64.f, -1.f, 1.f);
		} else {

			// center hole at north and south poles from surface to core

			auto centerdist = X * X + Y * Y;
			auto radius = 16.f + (abs(Z) * 0.1f) + Noise.GetPerlin_2D(X, Z, 0.01f) * 2.f;

			if (centerdist < radius) {
				Sample = FMath::Clamp(radius - centerdist, -1.f, 1.f);
			}

			// outer core
			if (dist < 129.f) {
				Sample = FMath::Clamp(128.f - dist, 0.05f, 1.f);

			}

		}


		// if (Sample < 0.0f) {
		//
		// 	Sample = Noise.GetSimplexFractal_3D(X,
		// 										Y,
		// 										Z, 0.001f, 8.f) * 0.25f - 0.2f;
		// }

	}

	// smooth gradient
	Sample /= 5;


	return Sample;
}

FVoxelMaterial FVoxelPlanetGeneratorInstance::GetMaterialImpl(v_flt X, v_flt Y, v_flt Z, int32 LOD,
															const FVoxelItemStack& Items) const {
	FVoxelMaterialBuilder MaterialBuilder;

	if (LayerIndex == static_cast<int>(PlanetLayerType::PLANET_LAYER_LIQUID)) {

		MaterialBuilder.SetMaterialConfig(EVoxelMaterialConfig::SingleIndex);
		MaterialBuilder.SetSingleIndex(0);

	} else {
		MaterialBuilder.SetMaterialConfig(EVoxelMaterialConfig::MultiIndex);


		// get density value from generator for this point
		// positive = empty
		// negative = full
		auto Sample = GetValueImpl(X, Y, Z, LOD, Items);
		float dist = FVector(X, Y, Z).Size();


		// AddMultiIndex
		//
		// Index		- Material index between 0 and 255
		// Strength 	- Strength, usually between 0 and 1
		// Lock Strength - If true, the strength won't be normalized
		//					For example, if you want small rocks with the same density everywhere


		// surface grass
		MaterialBuilder.AddMultiIndex(
			dist > SeaLevel + TerrainHeight * 0.8f ? 1 : 0,
			FMath::Clamp(Sample * -0.5f + 0.2f, 0.f, 1.f),
			false
		);

		// dirt and mud below the grass
		MaterialBuilder.AddMultiIndex(
			2,
			BlendRange(Sample, -0.2f, -2.0f, -4.f, -8.f),
			false
		);

		// stone
		MaterialBuilder.AddMultiIndex(
			3,
			BlendRange(Sample, -4.f, -8.f, -20.f, -20.f),
			false
		);

	}

	return MaterialBuilder.Build();
}

TVoxelRange<v_flt> FVoxelPlanetGeneratorInstance::GetValueRangeImpl(const FVoxelIntBox& bounds, int32 LOD,
																	const FVoxelItemStack& Items) const {
	// Return the values that GetValueImpl can return in Bounds
	// Used to skip chunks where the value does not change
	// Be careful, if wrong your world will have holes
	// By default return infinite range to be safe
	// return TVoxelRange<v_flt>::Infinite();

	// check if bounding box intersects the bounding sphere of the terrain

	// TODO: get planet center from generator settings
	const FIntVector PlanetCenter = FIntVector(0, 0, 0);

	// check if the center of the planet sphere is contained in the box
	if (bounds.Contains(PlanetCenter)) {
		// generate the chunk
		return TVoxelRange<v_flt>::Infinite();
	}

	// now we need to check if the sphere intersects with any of the sides of the box

	// get the closest point on the bounding box to the center of the planet sphere
	// and determine the squared distance from that point to the center of the sphere.
	auto ClosestDistanceSquared = bounds.ComputeSquaredDistanceFromBoxToPoint(PlanetCenter);

	// the maximum height of the generated terrain from the center of the planet
	// TODO: make sure the TerrainHeight + SeaLevel actually gives us the maximum height
	auto MaxDistanceSquared = pow(TerrainHeight + SecondaryTerrainHeight + SeaLevel, 2);


	// compare the radius of the sphere (the max terrain height) to the closest point distnance

	if (ClosestDistanceSquared <= MaxDistanceSquared) {
		// generate the chunk
		return TVoxelRange<v_flt>::Infinite();
	}

	// TODO do we need to actually calculate a min and max?
	return TVoxelRange<v_flt>(1, 2);


	// // Example for the GetValueImpl
	//
	// auto center = bounds.GetCenter();
	// FVector SamplePosition = FVector(center.X, center.Y, center.Z).GetSafeNormal();
	//
	// // Noise is between -1 and 1
	// TVoxelRange<v_flt> Value = TVoxelRange<v_flt>(-1, 1) * SamplePosition.Size();
	// // Z can go from min to max
	// // TVoxelRange<v_flt> Value = TVoxelRange<v_flt>(bounds.Min.Z, bounds.Max.Z) - Height;
	//
	// Value /= 20;
	//
	// return Value;
}

FVector FVoxelPlanetGeneratorInstance::GetUpVector(v_flt X, v_flt Y, v_flt Z) const {
	// used by spawners for orientation along terrain
	return FVector(X, Y, Z).GetSafeNormal();
}
