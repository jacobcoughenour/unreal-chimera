// Fill out your copyright notice in the Description page of Project Settings.


#include "GravityComponent.h"

#include "chimera/Planet.h"

// Sets default values for this component's properties
UGravityComponent::UGravityComponent() {
	// enable TickComponent()
	PrimaryComponentTick.bCanEverTick = true;

	// enable InitializeComponent()
	bWantsInitializeComponent = true;

}

void UGravityComponent::InitializeComponent() {
	Super::InitializeComponent();

	// get the collision component
	if (CollisionPrimitiveComponent == nullptr) {
		CollisionPrimitiveComponent = GetOwner()->FindComponentByClass<UPrimitiveComponent>();
	}
}


// Called every frame
void UGravityComponent::TickComponent(float DeltaTime, ELevelTick TickType,
									FActorComponentTickFunction* ThisTickFunction) {
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// skip update if there is no collision component to update
	if (CollisionPrimitiveComponent == nullptr)
		return;

	// disable normal gravity
	if (CollisionPrimitiveComponent->IsGravityEnabled())
		CollisionPrimitiveComponent->SetEnableGravity(false);

	// disable gravity if physics is disabled
	if (!CollisionPrimitiveComponent->IsSimulatingPhysics()) {
		Gravity = FVector::ZeroVector;
		return;
	}

	Gravity = APlanet::GetPlanetaryGravityAtPoint(this, CollisionPrimitiveComponent->GetComponentLocation())
			* CollisionPrimitiveComponent->GetMass();

	if (IsEffectedByGravity())
		CollisionPrimitiveComponent->BodyInstance.AddForce(Gravity, false);

}
