﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "ItemStack.h"

UItemStack::UItemStack(UItemStack& ItemStack): UItemStack(ItemStack.Item, ItemStack.ItemData, ItemStack.Amount) { }

UItemStack* UItemStack::Split(int32 SplitAmount) {
	const auto ActualSplitAmount = FMath::Min(SplitAmount, Amount);
	SetAmount(Amount - ActualSplitAmount);
	return MakeItemStack(GetOuter(), Item, ItemData, ActualSplitAmount);
}

UItem* UItemStack::GetItem() const {
	return Item;
}

int32 UItemStack::GetAmount() const {
	return Amount;
}

bool UItemStack::SetAmount(const int32 InAmount) {

	if (!Item->IsStackable)
		return InAmount == 1;

	if (InAmount < 0)
		return false;

	if (Amount != InAmount) {
		const auto Diff = InAmount - Amount;
		Amount = InAmount;

		NotifyItemStackAmountChanged(Diff);
	}

	return true;
}

FBlueprintJsonObject UItemStack::GetItemData() const {
	return ItemData;
}

void UItemStack::SetItemData(const FBlueprintJsonObject InItemData) {
	ItemData = InItemData;
	Item->OnItemDataChanged(this);
	NotifyItemStackDataChanged();
}

bool UItemStack::IsDamageable() {
	return Item->IsDamageable;
}

bool UItemStack::IsDamaged() {
	return Item->IsDamaged(this);
}

bool UItemStack::IsBroken() {
	return Item->IsBroken(this);
}

int32 UItemStack::GetDamage() {
	return Item->GetDamage(this);
}

void UItemStack::SetDamage(const int InDamage) {
	Item->SetDamage(this, InDamage);
}

void UItemStack::TakeDamage(const int InDamage) {
	Item->TakeDamage(this, InDamage);
}

int32 UItemStack::GetMaxDamage() {
	return Item->GetMaxDamage(this);
}

void UItemStack::OnActivation(AActor* InvokingActor) {
	Item->OnActivation(InvokingActor, this);
}

void UItemStack::OnDeactivation(AActor* InvokingActor) {
	Item->OnDeactivation(InvokingActor, this);
}

void UItemStack::Tick(AActor* InvokingActor) {
	Item->Tick(InvokingActor, this);
}

void UItemStack::OnPrimaryActionDown(AActor* InvokingActor) {
	Item->OnPrimaryActionDown(InvokingActor, this);
}

void UItemStack::OnPrimaryActionUp(AActor* InvokingActor) {
	Item->OnPrimaryActionUp(InvokingActor, this);
}

void UItemStack::OnSecondaryActionDown(AActor* InvokingActor) {
	Item->OnSecondaryActionDown(InvokingActor, this);
}

void UItemStack::OnSecondaryActionUp(AActor* InvokingActor) {
	Item->OnSecondaryActionUp(InvokingActor, this);
}

void UItemStack::NotifyItemStackAmountChanged(int32 AmountDifference) {

	// broadcast to native first
	OnItemStackAmountChangedNative.Broadcast(this, AmountDifference);
	OnItemStackAmountChanged.Broadcast(this, AmountDifference);

	// call blueprint event
	ItemStackAmountChanged(this, AmountDifference);
}

void UItemStack::NotifyItemStackDataChanged() {

	// broadcast to native first
	OnItemStackDataChangedNative.Broadcast(this);
	OnItemStackDataChanged.Broadcast(this);

	// call blueprint event
	ItemStackDataChanged(this);
}
