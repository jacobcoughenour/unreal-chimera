// Fill out your copyright notice in the Description page of Project Settings.


#include "ChimeraAssetManager.h"

#include "chimera.h"

const FPrimaryAssetType UChimeraAssetManager::ResourceItemType = TEXT("Resource");
const FPrimaryAssetType UChimeraAssetManager::ToolItemType = TEXT("Tool");

UChimeraAssetManager& UChimeraAssetManager::GetInstance() {
	UChimeraAssetManager* Instance = Cast<UChimeraAssetManager>(GEngine->AssetManager);

	if (Instance)
		return *Instance;

	UE_LOG(LogChimera, Fatal, TEXT("Invalid AssetManager in DefaultEngine.ini, must be ChimeraAssetManager!"));
	return *NewObject<UChimeraAssetManager>();
}

UItem* UChimeraAssetManager::ForceLoadItem(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning) {

	FSoftObjectPath ItemPath = GetPrimaryAssetPath(PrimaryAssetId);

	UItem* LoadedItem = Cast<UItem>(ItemPath.TryLoad());

	if (bLogWarning && LoadedItem == nullptr) {
		UE_LOG(LogChimera, Warning, TEXT("Failed to load item definition for identifier %s!"),
				*PrimaryAssetId.ToString());
	}

	return LoadedItem;
}
