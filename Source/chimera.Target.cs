// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class chimeraTarget : TargetRules {
	public chimeraTarget(TargetInfo Target) : base(Target) {
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange(new string[] {"chimera"});
	}
}