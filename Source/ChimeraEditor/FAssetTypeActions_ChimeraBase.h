﻿#pragma once

#include "CoreMinimal.h"
#include "AssetTypeActions_Base.h"

class FAssetTypeActions_ChimeraBase : public FAssetTypeActions_Base {
public:
	FAssetTypeActions_ChimeraBase(EAssetTypeCategories::Type AssetCategory) : AssetCategory(AssetCategory) { }

	virtual uint32 GetCategories() override { return AssetCategory; }

private:
	EAssetTypeCategories::Type AssetCategory;
};
