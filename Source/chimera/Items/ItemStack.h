﻿#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "../../../Plugins/BlueprintJson/Source/BlueprintJson/Classes/BlueprintJsonLibrary.h"
#include "Dom/JsonObject.h"
#include "Engine/DataAsset.h"
#include "Engine/EngineTypes.h"
#include "UObject/NoExportTypes.h"
#include "ItemStack.generated.h"

class UItem;
class UItemStack;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnItemStackAmountChanged, UItemStack*, ItemStack, int32, AmountDifference)
;

DECLARE_MULTICAST_DELEGATE_TwoParams(FOnItemStackAmountChangedNative, UItemStack*, int32);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnItemStackDataChanged, UItemStack*, ItemStack)
;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnItemStackDataChangedNative, UItemStack*);


/**
* Item Stack
*/
UCLASS(BlueprintType)
class CHIMERA_API UItemStack : public UObject {

	GENERATED_BODY()

private:

	UPROPERTY()
	UItem* Item;

	UPROPERTY()
	FBlueprintJsonObject ItemData;

	int32 Amount;

	FTimerHandle ItemPrimaryActionTimerHandle;
	FTimerHandle ItemSecondaryActionTimerHandle;

public:

	UItemStack() : UObject() { }

	explicit UItemStack(UItem* InItem, FBlueprintJsonObject InItemData, const int32 InAmount = 1) :
		UObject(),
		Item(InItem),
		ItemData(InItemData),
		Amount(InItem->IsStackable ? FMath::Max(InAmount, 1) : 1) { }

	UItemStack(UItemStack& ItemStack);

	UFUNCTION(BlueprintPure)
	static UItemStack* MakeItemStack(
		UObject* Outer,
		UItem* InItem,
		const FBlueprintJsonObject InItemData,
		const int32 InAmount = 1
	) {
		if (InItem == nullptr)
			return nullptr;

		const auto ItemStack = NewObject<UItemStack>(Outer, StaticClass());

		ItemStack->Item = InItem;
		ItemStack->ItemData = InItemData;
		ItemStack->Item->OnInitializeItemData(ItemStack->ItemData);
		ItemStack->Amount = InItem->IsStackable ? FMath::Max(InAmount, 1) : 1;

		ItemStack->Item->OnConstruction(ItemStack);
		return ItemStack;
	}

	UFUNCTION(BlueprintCallable)
	UItemStack* Split(int32 SplitAmount);

	UFUNCTION(BlueprintPure)
	UItem* GetItem() const;

	UFUNCTION(BlueprintPure)
	int32 GetAmount() const;

	UFUNCTION(BlueprintCallable)
	bool SetAmount(const int32 InAmount);

	UFUNCTION(BlueprintPure)
	FBlueprintJsonObject GetItemData() const;

	UFUNCTION(BlueprintCallable)
	void SetItemData(const FBlueprintJsonObject InItemData);

	UFUNCTION(BlueprintPure)
	bool IsDamageable();

	UFUNCTION(BlueprintPure)
	bool IsDamaged();

	UFUNCTION(BlueprintPure)
	bool IsBroken();

	UFUNCTION(BlueprintPure)
	int32 GetDamage();

	UFUNCTION(BlueprintCallable)
	void SetDamage(const int InDamage);

	UFUNCTION(BlueprintCallable)
	void TakeDamage(const int InDamage);

	UFUNCTION(BlueprintPure)
	int32 GetMaxDamage();

	UFUNCTION(BlueprintCallable)
	void OnActivation(AActor* InvokingActor);

	UFUNCTION(BlueprintCallable)
	void OnDeactivation(AActor* InvokingActor);

	UFUNCTION(BlueprintCallable)
	void Tick(AActor* InvokingActor);

	UFUNCTION(BlueprintCallable)
	void OnPrimaryActionDown(AActor* InvokingActor);

	UFUNCTION(BlueprintCallable)
	void OnPrimaryActionUp(AActor* InvokingActor);

	UFUNCTION(BlueprintCallable)
	void OnSecondaryActionDown(AActor* InvokingActor);

	UFUNCTION(BlueprintCallable)
	void OnSecondaryActionUp(AActor* InvokingActor);

	FTimerHandle& GetItemPrimaryActionTimerHandle() {
		return ItemPrimaryActionTimerHandle;
	}

	FTimerHandle& GetItemSecondaryActionTimerHandle() {
		return ItemSecondaryActionTimerHandle;
	}

protected:

	void NotifyItemStackAmountChanged(int32 AmountDifference);
	void NotifyItemStackDataChanged();

public:

	UFUNCTION(BlueprintImplementableEvent)
	void ItemStackAmountChanged(UItemStack* ItemStack, int32 AmountDifference);

	/**
	* Delegate called the item stack amount is changed
	*/
	UPROPERTY(BlueprintAssignable)
	FOnItemStackAmountChanged OnItemStackAmountChanged;

	FOnItemStackAmountChangedNative OnItemStackAmountChangedNative;


	UFUNCTION(BlueprintImplementableEvent)
	void ItemStackDataChanged(UItemStack* ItemStack);

	/**
	* Delegate called the item stack data is changed
	*/
	UPROPERTY(BlueprintAssignable)
	FOnItemStackDataChanged OnItemStackDataChanged;

	FOnItemStackDataChangedNative OnItemStackDataChangedNative;

};
