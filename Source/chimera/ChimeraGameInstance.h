// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AdvancedFriendsGameInstance.h"
#include "CoreMinimal.h"
#include "Kismet/GameplayStatics.h"
#include "Planet.h"

#include "ChimeraGameInstance.generated.h"

/**
 * Chimera Game Instance
 */
UCLASS()
class CHIMERA_API UChimeraGameInstance : public UAdvancedFriendsGameInstance {
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadOnly)
	TSet<APlanet*> RegisteredPlanets;

	UFUNCTION()
	void RegisterPlanet(APlanet* Planet) {
		RegisteredPlanets.Add(Planet);
	}

	UFUNCTION()
	void UnregisterPlanet(APlanet* Planet) {
		RegisteredPlanets.Remove(Planet);
	}

	UFUNCTION(BlueprintPure)
	static UChimeraGameInstance* GetChimeraGameInstance(const UObject* WorldContextObject) {

		UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull);
		if (World == nullptr)
			return nullptr;

		return Cast<UChimeraGameInstance>(World->GetGameInstance());
	}

};
