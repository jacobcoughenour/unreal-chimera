#pragma once

#include "CoreMinimal.h"
#include "VoxelFastNoise.h"
#include "VoxelWorldGenerators/VoxelWorldGeneratorHelpers.h"
#include "PlanetGenerator.generated.h"

UCLASS(Blueprintable)
class CHIMERA_API UPlanetGenerator : public UVoxelWorldGenerator {
	GENERATED_BODY()

public:
	virtual TMap<FName, int32> GetDefaultSeeds() const override;
	virtual TVoxelSharedRef<FVoxelWorldGeneratorInstance> GetInstance() override;
};

UENUM(BlueprintType)
enum class PlanetLayerType: uint8 {
	PLANET_LAYER_SOLID = 0,
	PLANET_LAYER_LIQUID = 1,
	PLANET_LAYER_COUNT UMETA(Hidden)
};

class CHIMERA_API FVoxelPlanetGeneratorInstance : public TVoxelWorldGeneratorInstanceHelper<
		FVoxelPlanetGeneratorInstance, UPlanetGenerator> {
public:
	explicit FVoxelPlanetGeneratorInstance(
		const UPlanetGenerator& PlanetGenerator);

	virtual void Init(const FVoxelWorldGeneratorInit& InitStruct) override;

	v_flt GetValueImpl(v_flt X, v_flt Y, v_flt Z, int32 LOD,
						const FVoxelItemStack& Items) const;
	FVoxelMaterial GetMaterialImpl(v_flt X, v_flt Y, v_flt Z, int32 LOD,
									const FVoxelItemStack& Items) const;

	TVoxelRange<v_flt> GetValueRangeImpl(const FVoxelIntBox& bounds, int32 LOD,
										const FVoxelItemStack& Items) const;

	virtual FVector GetUpVector(v_flt X, v_flt Y, v_flt Z) const override final;


	static CONSTEXPR FORCEINLINE float BlendRange(
		const float Sample,
		const float BlendInStart,
		const float BlendInEnd,
		const float BlendOutStart,
		const float BlendOutEnd
	) {

		// before blend in
		if (Sample >= BlendInStart)
			return 0.f;

		// blend in
		if (Sample > BlendInEnd) {
			if (BlendInStart <= BlendInEnd)
				return 1.f;
			return (BlendInStart - Sample) / (BlendInStart - BlendInEnd);
		}

		// before blend out
		if (Sample > BlendOutStart)
			return 1.f;

		// blend out
		if (Sample > BlendOutEnd) {
			if (BlendOutStart <= BlendOutEnd)
				return 1.f;
			return 1.f - (BlendOutStart - Sample) / (BlendOutStart - BlendOutEnd);
		}

		return 0.f;
	}

private:
	FVoxelFastNoise Noise;
	int32 LayerIndex;
	int32 SeaLevel;
	int32 TerrainHeight;
	int32 SecondaryTerrainHeight;
	int32 NoiseExponent;
	int32 NoiseFrequency;
	int32 SecondaryNoiseFrequency;
	int32 NoiseOctaves;
	int32 SecondaryNoiseOctaves;
};
