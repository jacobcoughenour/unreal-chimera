﻿#pragma once

#include "CoreMinimal.h"
#include "../../../Plugins/BlueprintJson/Source/BlueprintJson/Classes/BlueprintJsonLibrary.h"
#include "Engine/DataAsset.h"
#include "UObject/NoExportTypes.h"
#include "UObject/Interface.h"
#include "Item.generated.h"

class UItemStack;


UINTERFACE(MinimalAPI)
class UItemInvokingActor : public UInterface {
	GENERATED_BODY()
};

class CHIMERA_API IItemInvokingActor {
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FTwoVectors GetViewRay();

	// UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	// bool IsPrimaryActionDown();
	//
	// UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	// bool IsSecondaryActionDown();

};

/**
 * Item base class
 */
UCLASS(Abstract, BlueprintType)
class CHIMERA_API UItem : public UPrimaryDataAsset {

	GENERATED_BODY()

public:

	UItem() {}

	/**
	 * Item Type
	 */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Item)
	FPrimaryAssetType Type;

	/**
	 * Item Name displayed in-game
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Item)
	FText Name;

	/**
	 * Item Description displayed in-game
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Item)
	FText Description;

	/**
	 * Icon displayed in menus to represent this item
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Item)
	FSlateBrush Icon;

	/**
	 * Mesh used to represent the item in the world
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Item)
	UStaticMesh* WorldMesh = nullptr;

	/**
	 * Mesh shown in player's hands
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Item)
	USkeletalMesh* HeldMesh = nullptr;

	/**
	* If instances of this item can be stacked together
	*/
	UPROPERTY(BlueprintReadOnly, Category=Item)
	bool IsStackable = false;

	/**
	* If instances of this item can be damaged
	*/
	UPROPERTY(BlueprintReadOnly, Category=Item)
	bool IsDamageable = false;

	/**
	 * The initial delay between OnPrimaryActionDown() and DoPrimaryAction() in seconds.
	 *
	 * <= 0 disables this delay.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Actions)
	float PrimaryActionInitialDelay = 0.0f;

	/**
	 * The rate at which DoPrimaryAction() is called while the input is held down in seconds.
	 *
	 * <= 0 disables this functionality (primary action is only fired once per input press)
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Actions)
	float PrimaryActionFireRate = 0.0f;

	/**
	* The initial delay between OnSecondaryActionDown() and DoSecondaryAction() in seconds.
	*
	* <= 0 disables this delay.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Actions)
	float SecondaryActionInitialDelay = 0.0f;

	/**
	* The rate at which DoSecondaryAction() is called while the input is held down in seconds.
	*
	* <= 0 disables this functionality (secondary action is only fired once per input press)
	*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Actions)
	float SecondaryActionFireRate = 0.0f;


	UFUNCTION(BlueprintCallable, Category=Item)
	FString GetIdentifierString() const;

	virtual FPrimaryAssetId GetPrimaryAssetId() const override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	bool TraceInvokerView(struct FHitResult& HitResult, AActor* InvokingActor, const float Distance = 640.0f);

	virtual bool TraceInvokerView_Implementation(struct FHitResult& HitResult, AActor* InvokingActor,
												const float Distance = 640.0f);;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	bool IsDamaged(UItemStack* ItemStack);
	virtual bool IsDamaged_Implementation(UItemStack* ItemStack);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	bool IsBroken(UItemStack* ItemStack);
	virtual bool IsBroken_Implementation(UItemStack* ItemStack);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	int32 GetDamage(UItemStack* ItemStack);
	virtual int32 GetDamage_Implementation(UItemStack* ItemStack);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SetDamage(UItemStack* ItemStack, const int InDamage);
	virtual void SetDamage_Implementation(UItemStack* ItemStack, const int InDamage);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void TakeDamage(UItemStack* ItemStack, const int InDamage);
	virtual void TakeDamage_Implementation(UItemStack* ItemStack, const int InDamage);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	int32 GetMaxDamage(UItemStack* ItemStack);
	virtual int32 GetMaxDamage_Implementation(UItemStack* ItemStack);

	/**
	 * Used when creating a new ItemStack of this Item to get the default ItemData values.
	 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FBlueprintJsonObject GetDefaultItemData();
	virtual FBlueprintJsonObject GetDefaultItemData_Implementation();

	/**
	 * Called while an ItemStack is being created of this Item to initialize the stack's ItemData.
	 *
	 * The output ItemData is the result of the input ItemData values being assigned on to the defaults from GetDefaultItemData()
	 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnInitializeItemData(struct FBlueprintJsonObject& ItemData);
	virtual void OnInitializeItemData_Implementation(struct FBlueprintJsonObject& ItemData);

	/**
	 * Called right after an ItemStack of this Item has been created.
	 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnConstruction(UItemStack* ItemStack);
	virtual void OnConstruction_Implementation(UItemStack* ItemStack);

	/**
	 * Called after ItemData was changed for this Item via UItemStack::SetItemData()
	 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnItemDataChanged(UItemStack* ItemStack);
	virtual void OnItemDataChanged_Implementation(UItemStack* ItemStack);

	/**
	 * Called when the item is held by a player
	 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnActivation(AActor* InvokingActor, UItemStack* ItemStack);
	virtual void OnActivation_Implementation(AActor* InvokingActor, UItemStack* ItemStack);

	/**
	 * Called when the item is no longer held by the player
	 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnDeactivation(AActor* InvokingActor, UItemStack* ItemStack);
	virtual void OnDeactivation_Implementation(AActor* InvokingActor, UItemStack* ItemStack);

	/**
	 * Called from the player tick of the player holding the item
	 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Tick(AActor* InvokingActor, UItemStack* ItemStack);
	virtual void Tick_Implementation(AActor* InvokingActor, UItemStack* ItemStack);


	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DoPrimaryAction(AActor* InvokingActor, UItemStack* ItemStack, FHitResult HitResult);
	virtual void DoPrimaryAction_Implementation(AActor* InvokingActor, UItemStack* ItemStack, FHitResult HitResult);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DoSecondaryAction(AActor* InvokingActor, UItemStack* ItemStack, FHitResult HitResult);
	virtual void DoSecondaryAction_Implementation(AActor* InvokingActor, UItemStack* ItemStack, FHitResult HitResult);


	/**
	 * Called when a player starts pressing the primary action input key
	 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnPrimaryActionDown(AActor* InvokingActor, UItemStack* ItemStack);
	virtual void OnPrimaryActionDown_Implementation(AActor* InvokingActor, UItemStack* ItemStack);

	/**
	 * Called when a player releases the primary action input key
	 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnPrimaryActionUp(AActor* InvokingActor, UItemStack* ItemStack);
	virtual void OnPrimaryActionUp_Implementation(AActor* InvokingActor, UItemStack* ItemStack);

	/**
	 * Called when a player starts pressing the secondary action input key
	 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnSecondaryActionDown(AActor* InvokingActor, UItemStack* ItemStack);
	virtual void OnSecondaryActionDown_Implementation(AActor* InvokingActor, UItemStack* ItemStack);

	/**
	 * Called when a player releases the secondary action input key
	 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnSecondaryActionUp(AActor* InvokingActor, UItemStack* ItemStack);
	virtual void OnSecondaryActionUp_Implementation(AActor* InvokingActor, UItemStack* ItemStack);


};
