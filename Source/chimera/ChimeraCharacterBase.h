﻿#pragma once

#include "CoreMinimal.h"
#include "VoxelCharacter.h"
#include "DirGravity/Public/GravityMovementComponent.h"
#include "GameFramework/Character.h"
#include "ChimeraCharacterBase.generated.h"


UCLASS()
class CHIMERA_API AChimeraCharacterBase : public AVoxelCharacter {
	GENERATED_BODY()

public:
	AChimeraCharacterBase(const FObjectInitializer& ObjectInitializer) : Super(
		ObjectInitializer.SetDefaultSubobjectClass<UGravityMovementComponent>(
			ACharacter::CharacterMovementComponentName)) {};

protected:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Character")
	UGravityMovementComponent* GetGravityMovementComponent() {
		return Cast<UGravityMovementComponent>(GetMovementComponent());
	}

};
