// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Interactable.generated.h"

UINTERFACE(MinimalAPI)
class UInteractable : public UInterface {
	GENERATED_BODY()
};

class CHIMERA_API IInteractable {
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnBeginInteraction(const AActor* InvokingActor);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnEndInteraction(const AActor* InvokingActor);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FText GetActionText(const AActor* InvokingActor);
};
